#zad1
list1=[1, 5, 30, 0, 20, 3]
list2=[5, 30, 1, 89, 27, 3, 1, 5]

def function(listVal):
    maxVal=-999
    minVal=999
    average=0
    counter=0
    for i in listVal:
        if i>maxVal:
            maxVal=i
        if i<minVal:
            minVal=i

        average+=i
        counter+=1
    print(maxVal)
    print(minVal)
    print(average/counter)

    
function(list1)


#zad2
listaKopiowana = []
def copyFun(lista):
    for i in lista:
        listaKopiowana.append(i)

    print(listaKopiowana)

copyFun(list1)


#zad3

def thirdFun(lista):
    averageList= []
    averageList.append(lista[0])
    for i in range(1, (len(lista)-2)):
        averageList.append(((lista[i]+lista[i+1]+lista[i-1])/3))
    averageList.append(lista[len(lista)-1])
    print(averageList)

thirdFun(list1)

#zad4

def fourthFun(lista1, lista2):
    lista3 = []
    limit=0
    if len(lista1)>=len(lista2):
        limit=len(lista2)
    else:
        limit=len(lista1)
        
    for i in range(0, limit):
        lista3.append((lista1[i]+lista2[i]))

    print(lista3)



fourthFun(list1, list2)

#zad5

def fifthFun(lista1, lista2):
    lista3 = []
    limit=0
    if len(lista1)>=len(lista2):
        limit=len(lista2)
    else:
        limit=len(lista1)
        
    for i in range(0, limit):
        if (lista1[i]==0 or lista2[i]==0):
            pass
        else:
            lista3.append((lista1[i]/lista2[i]))

    print(lista3)



fifthFun(list1, list2)
